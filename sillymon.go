package main

import (
    "fmt"
    "os"
    "log"
    "io/ioutil"
    "os/exec"
    "path"
)


func help() {
  fmt.Println(" Use one of these variable:")
  fmt.Println("   cron    :    run the same as cron but you can see STDOUT")
  fmt.Println("   status  :    show output from last checks")
  fmt.Println(" ")
  os.Exit(1)
}

func status() {
  fmt.Println("Check status")
}

func fLastLine(fname string) {
  file, err := os.Open(fname)
  if err != nil { panic(err) }
  defer file.Close()

	buf := make([]byte, 62)
  stat, err := os.Stat(fname)
  start := stat.Size() - 62
  _, err = file.ReadAt(buf, start)

  if err == nil {
    fmt.Printf("%s\n", buf)
  }
}


func cron(check_path string) {
  fmt.Println("Run all checks by user")
  files, err := ioutil.ReadDir(check_path)
  if err != nil {
    log.Fatal(err)
    os.Exit(1)
  }


  for _, file := range files {
    fmt.Println("Executing", file.Name())
    check := path.Join(check_path, file.Name())
    out, err := exec.Command(check).Output()
    if err != nil {
      fmt.Println("ERR:", err)
    }
    // check if last_state file exists and read last line
		// than compare last result and if is different notify admin
		if _, err := os.Stat(check); err == nil {
      
    }
		// finaly save result to state file
    fmt.Printf("%s", out)
  }

}


func main() {
  var CHECKS string = "./checks"

  if len(os.Args) == 2 {
    switch os.Args[1] {
      case "cron"   : cron(CHECKS)
      case "status" : status()
      default       : help()
    }
  } else {
    help()
  }
}
